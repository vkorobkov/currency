# Cuncurrency #

## How to build

What is needed:

* Java8
* Maven
* MySQL
* Apache Tomcat 7+

Steps:

1. Clone repository: git clone https://vkorobkov@bitbucket.org/vkorobkov/currency.git
2. Create an empty schema in mysql: create database currency_test default character set = utf8 default collate = utf8_general_ci;
3. Edit database connection properties(spring.datasource.url, spring.datasource.username, spring.datasource.password) in application.properties (src/main/resources/application.properties)
4. Build without tests: mvn clean install -DskipTests
5. Copy WAR file into CATALINA_HOME/webapps
6. Rename WAR file into ROOT.WAR
7. Start apache tomcat
8. Goto UI page - http://localhost:8080/index/


## Performance

Managed to achive 200+ IOPS on Amazon micro instance with 900MB of RAM. Tests were performed on the same machine. Test client used to sent requests in 3 parallel threads. No MySql/Java optimizations were performed.

Test client code: https://bitbucket.org/vkorobkov/currency/src/48bd869d8e866ba129a1021f3703f41db0821858/src/test/java/com/vkorobkov/integration/MultiThreadLongPostingTest.java?at=master

Some tricky data structure was introduced to achieve such performance:
https://bitbucket.org/vkorobkov/currency/src/48bd869d8e866ba129a1021f3703f41db0821858/src/main/java/org/vkorobkov/currency/service/TradeMessageService.java?at=master


## UI

UI updates automatically using WebSockets. It updates:

* Current speed - an amount of IOPS
* Total number of performed transactions
* Last 100 transactions