package org.vkorobkov.currency.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import org.vkorobkov.currency.util.TradeMessage;

@Getter
public class TradeMessageReceivedEvent extends ApplicationEvent {

    private final TradeMessage tradeMessage;

    public TradeMessageReceivedEvent(Object source, TradeMessage tradeMessage) {
        super(source);
        this.tradeMessage = tradeMessage;
    }
}
