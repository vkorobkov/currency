package org.vkorobkov.currency.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Objects;

@Repository
public class TradeMessagesDao {

    private static final String TABLE_NAME = "trade_message";

    @Autowired
    private DataSource dataSource;

    // Yes - I am passing map not DTO just to perform DTO->Map transformations out of the transaction
    @Transactional(readOnly = false)
    public void writeTradeMessages(Map<String, Objects> ... tradeMessageData) {
        new SimpleJdbcInsert(dataSource).withTableName(TABLE_NAME).executeBatch(tradeMessageData);
    }
}
