package org.vkorobkov.currency.util;

// Top 10 currencies. Took from here http://www.whichcountry.co/top-ten-currencies-in-the-world/
public enum Currency {
    USD, EUR, JPY, GBP, CHF, AUD, CAD, SEK, HKD, NOK
}
