package org.vkorobkov.currency.util;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import org.vkorobkov.currency.util.serialize.CurrencyJsonSerializer;
import org.vkorobkov.currency.util.serialize.DateDeserializer;
import org.vkorobkov.currency.util.serialize.RateJsonSerializer;
import org.vkorobkov.currency.util.serialize.UppercaseDateSerializer;

import java.util.Date;

@Getter
@Setter
public class TradeMessage {

    private String userId;

    private Currency currencyFrom;

    private Currency currencyTo;

    @JsonSerialize(using = CurrencyJsonSerializer.class)
    private double amountSell;

    @JsonSerialize(using = CurrencyJsonSerializer.class)
    private double amountBuy;

    @JsonSerialize(using = RateJsonSerializer.class)
    private double rate;

    @JsonSerialize(using = UppercaseDateSerializer.class)
    @JsonDeserialize(using = DateDeserializer.class)
    private Date timePlaced;

    private CountryCode originatingCountry;
}
