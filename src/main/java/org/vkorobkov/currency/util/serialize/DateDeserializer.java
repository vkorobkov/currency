package org.vkorobkov.currency.util.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateDeserializer extends JsonDeserializer<Date> {

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
    private final StringDeserializer stringDeserializer = new StringDeserializer();
    private final Object lock = new Object();

    @Override
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        String date = stringDeserializer.deserialize(jp, ctxt);
        try {
            Date result;
            synchronized (lock) { // Because DateFormat is not thread safe
                result = dateFormat.parse(date);
            }
            return result;
        } catch (ParseException e) {
            throw new RuntimeException("Can not parse invalid date format: " + date);
        }
    }
}