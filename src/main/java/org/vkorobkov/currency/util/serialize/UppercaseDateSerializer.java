package org.vkorobkov.currency.util.serialize;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class UppercaseDateSerializer extends JsonSerializer<Date> {

    private final DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
    private final Object lock = new Object();

    @Override
    public void serialize(Date value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        String strValue;
        synchronized (lock) { // Because DateFormat is not thread safe
            strValue = dateFormat.format(value);
        }
        jgen.writeString(strValue.toUpperCase());
    }
}
