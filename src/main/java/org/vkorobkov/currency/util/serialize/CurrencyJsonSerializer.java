package org.vkorobkov.currency.util.serialize;


public class CurrencyJsonSerializer extends RoundJsonSerializer {
    public CurrencyJsonSerializer() {
        super("#.##");
    }
}
