package org.vkorobkov.currency.util.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.DecimalFormat;


public abstract class RoundJsonSerializer extends JsonSerializer<Double> {

    private final DecimalFormat digitalFormat;

    private final Object lock = new Object();

    public RoundJsonSerializer(String format) {
        this.digitalFormat = new DecimalFormat(format);
    }

    @Override
    public void serialize(Double value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        String strValue;
        // The synchronization is required because DecimalFormat is not thread self itself. JavaDoc:
        // "If multiple threads access a format concurrently, it must be synchronized
        // externally"
        synchronized (lock) {
            strValue = digitalFormat.format(value);
        }
        jgen.writeString(strValue);
    }
}
