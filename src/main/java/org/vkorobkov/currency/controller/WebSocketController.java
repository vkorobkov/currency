package org.vkorobkov.currency.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.vkorobkov.currency.service.IopsTrendService;
import org.vkorobkov.currency.service.Last100MessagesService;

@Controller
public class WebSocketController {

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private IopsTrendService iopsTrendService;

    @Autowired
    private Last100MessagesService last100MessagesService;

    @Scheduled(fixedRate=1000)
    public void sendTrend() throws JsonProcessingException {
        TrendDTO trend = new TrendDTO(iopsTrendService.getLastIops(), iopsTrendService.getTotal());
        this.template.convertAndSend("/topic/trend", trend);
    }

    @Scheduled(fixedRate=1000)
    public void sendLastMessages() {
        this.template.convertAndSend("/topic/last", last100MessagesService.getLastTradeMessages());
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    private static class TrendDTO {
        private int iops;
        private long total;
    }
}
