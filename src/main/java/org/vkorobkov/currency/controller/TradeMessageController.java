package org.vkorobkov.currency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.vkorobkov.currency.service.TradeMessageService;
import org.vkorobkov.currency.util.TradeMessage;

@Controller
public class TradeMessageController {

    @Autowired
    private TradeMessageService tradeMessageService;

    @RequestMapping(value = "/trade-message/", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public void index(@RequestBody TradeMessage tradeMessage) {
        tradeMessageService.writeTradeMessage(tradeMessage);
    }
}
