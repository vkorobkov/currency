package org.vkorobkov.currency.service;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.vkorobkov.currency.event.TradeMessageReceivedEvent;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

// Service for monitoring the trend of IOPS - operations per second
@Service
public class IopsTrendService implements ApplicationListener<TradeMessageReceivedEvent> {

    private final AtomicInteger iops = new AtomicInteger(0);
    private final AtomicLong total = new AtomicLong(0);
    private volatile int lastIops;

    @Override
    public void onApplicationEvent(TradeMessageReceivedEvent event) {
        iops.incrementAndGet();
        total.incrementAndGet();
    }

    @Scheduled(fixedRate=1000)
    public void resetAmount() {
        lastIops = iops.getAndSet(0);
    }

    public int getLastIops() {
        return lastIops;
    }

    public long getTotal() {
        return total.get();
    }
}
