package org.vkorobkov.currency.service;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import org.vkorobkov.currency.event.TradeMessageReceivedEvent;
import org.vkorobkov.currency.util.TradeMessage;

import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class Last100MessagesService  implements ApplicationListener<TradeMessageReceivedEvent> {

    private final ConcurrentLinkedQueue<TradeMessage> last100Events = new ConcurrentLinkedQueue<>();

    @Override
    public void onApplicationEvent(TradeMessageReceivedEvent event) {
        last100Events.add(event.getTradeMessage());

        while(last100Events.size() > 100) {
            last100Events.remove();
        }
    }

    public Object[] getLastTradeMessages() {
        return last100Events.toArray();
    }
}
