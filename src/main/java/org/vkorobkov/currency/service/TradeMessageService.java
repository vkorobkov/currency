package org.vkorobkov.currency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.vkorobkov.currency.dao.TradeMessagesDao;
import org.vkorobkov.currency.event.TradeMessageReceivedEvent;
import org.vkorobkov.currency.util.TradeMessage;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class TradeMessageService implements ApplicationEventPublisherAware {

    private final static int MAX_POOL_SIZE = 50;

    @Autowired
    private TradeMessagesDao tradeMessagesDao;

    // Non blocking queue
    private volatile ConcurrentLinkedQueue<TradeMessage> bufferQueue = new ConcurrentLinkedQueue<>();

    private final Object bufferQueueLock = new Object();
    private ApplicationEventPublisher applicationEventPublisher;

    public void writeTradeMessage(TradeMessage tradeMessage) {
        bufferQueue.add(tradeMessage);
        flushOnDiskIfSizeGreaterThan(MAX_POOL_SIZE);

        applicationEventPublisher.publishEvent(new TradeMessageReceivedEvent(this, tradeMessage));
    }

    @Scheduled(fixedRate=1000)
    public void flushOnDiskEachSecond() {
        flushOnDiskIfSizeGreaterThan(0);
    }

    private void flushOnDiskIfSizeGreaterThan(int maxSize) {
        if (bufferQueue.size() > maxSize) {
            ConcurrentLinkedQueue<TradeMessage> toProcess = null;
            synchronized (bufferQueueLock) {
                if (bufferQueue.size() > maxSize) {
                    toProcess = bufferQueue;
                    bufferQueue = new ConcurrentLinkedQueue<>();
                }
            }

            if (toProcess != null) {
                processBufferOfTradeMessages(toProcess);
            }
        }
    }

    private void processBufferOfTradeMessages(ConcurrentLinkedQueue<TradeMessage> tradeMessages) {
        int size = tradeMessages.size();
        Map parameters[] = new Map[size];
        for (int j = 0; j < size; ++j) {
            parameters[j] = buildParametersMap(tradeMessages.poll());
        }

        tradeMessagesDao.writeTradeMessages(parameters);
    }

    private Map<String, Object> buildParametersMap(TradeMessage tradeMessage) {
        Map parameters = new HashMap<>(8);
        parameters.put("user_id", tradeMessage.getUserId());
        parameters.put("currency_from", tradeMessage.getCurrencyFrom());
        parameters.put("currency_to", tradeMessage.getCurrencyTo());
        parameters.put("amount_sell", tradeMessage.getAmountSell());
        parameters.put("amount_buy", tradeMessage.getAmountBuy());
        parameters.put("rate", tradeMessage.getRate());
        parameters.put("time_placed", tradeMessage.getTimePlaced());
        parameters.put("originating_country", tradeMessage.getOriginatingCountry());
        return parameters;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
