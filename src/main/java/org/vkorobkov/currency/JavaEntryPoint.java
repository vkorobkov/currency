package org.vkorobkov.currency;

import org.springframework.boot.SpringApplication;

public class JavaEntryPoint {
    public static void main(String[] args) {
        SpringApplication.run(RootJavaConfig.class, args);
    }
}
