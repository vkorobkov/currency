ALTER TABLE `trade_message`
	ALTER `amount_cell` DROP DEFAULT;
ALTER TABLE `trade_message`
	CHANGE COLUMN `amount_cell` `amount_sell` DECIMAL(10,0) NOT NULL AFTER `currency_to`;
