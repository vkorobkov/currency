CREATE TABLE `trade_message` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`currencyFrom` VARCHAR(3) NOT NULL,
	`currencyTo` VARCHAR(3) NOT NULL,
	`amountCell` DECIMAL(10,0) NOT NULL,
	`amoutBuy` DECIMAL(10,0) NOT NULL,
	`rate` FLOAT NOT NULL,
	`timePlaced` TIMESTAMP NOT NULL,
	`originatingCountr` VARCHAR(3) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
