ALTER TABLE `trade_message`
	ALTER `userId` DROP DEFAULT,
	ALTER `currencyFrom` DROP DEFAULT,
	ALTER `currencyTo` DROP DEFAULT,
	ALTER `amountCell` DROP DEFAULT,
	ALTER `amoutBuy` DROP DEFAULT,
	ALTER `originatingCountr` DROP DEFAULT;
ALTER TABLE `trade_message`
	CHANGE COLUMN `userId` `user_id` VARCHAR(20) NOT NULL AFTER `id`,
	CHANGE COLUMN `currencyFrom` `currency_from` VARCHAR(3) NOT NULL AFTER `user_id`,
	CHANGE COLUMN `currencyTo` `currency_to` VARCHAR(3) NOT NULL AFTER `currency_from`,
	CHANGE COLUMN `amountCell` `amount_cell` DECIMAL(10,0) NOT NULL AFTER `currency_to`,
	CHANGE COLUMN `amoutBuy` `amout_buy` DECIMAL(10,0) NOT NULL AFTER `amount_cell`,
	CHANGE COLUMN `timePlaced` `time_placed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `rate`,
	CHANGE COLUMN `originatingCountr` `originating_country` VARCHAR(3) NOT NULL AFTER `time_placed`;
