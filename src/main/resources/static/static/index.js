
(function entryPoint() {

    function connect() {
        var socket = new SockJS('/websock');
        var stompClient = Stomp.over(socket);
        var dynatable = null;
        stompClient.connect({}, function() {
            stompClient.subscribe('/topic/trend', function(trend){
                trend = JSON.parse(trend.body);
                $("#iops_container").text('IOPS: ' + trend.iops);
                $("#total_transactions").text('Total transactions: ' + trend.total);
            });

            stompClient.subscribe('/topic/last', function(last){
                var lastBody = JSON.parse(last.body);

                if (!lastBody) {
                    return;
                }

                if (dynatable == null) {
                    dynatable = $('#last-messages-container').dynatable({
                        dataset: {
                            records: lastBody
                        },
                        features: {
                            paginate: false,
                            search: false,
                            recordCount: false,
                            perPageSelect: false,
                            sorting: false
                        }
                    }).data('dynatable');
                }
                else {
                    dynatable.records.updateFromJson({
                        records: lastBody
                    });
                    dynatable.dom.update();
                }
            });
        });
    };

    connect();
})();