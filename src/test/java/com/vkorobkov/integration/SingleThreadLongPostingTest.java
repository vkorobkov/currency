package com.vkorobkov.integration;


import com.vkorobkov.integration.utils.TradeMessagePoster;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

@Slf4j
public class SingleThreadLongPostingTest {

    private final static int TIMES_TO_POST = 1000;

    @Test
    public void postManyTimes() throws IOException {
        long timeStart = System.currentTimeMillis();

        CloseableHttpClient client = HttpClients.createDefault();
        for (int j = 0; j < TIMES_TO_POST; ++j) {
            log.info("About to post");
            int responseCode = TradeMessagePoster.postRandomTradeMessage(client);
            log.info("Just posted");
            Assert.assertEquals(200, responseCode);
        }

        long timeTook = System.currentTimeMillis() - timeStart;
        log.warn("IOPS: " + (int)((double)TIMES_TO_POST / ((double)timeTook / 1000.0) ));
    }
}
