package com.vkorobkov.integration.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.vkorobkov.currency.util.CountryCode;
import org.vkorobkov.currency.util.Currency;
import org.vkorobkov.currency.util.TradeMessage;

import java.util.Date;


public class RandomTradeMessageGenerator {

    public TradeMessage generateRandomTradeMessage() {
        TradeMessage tradeMessage = new TradeMessage();
        tradeMessage.setUserId(generateRandomUserId());
        tradeMessage.setCurrencyFrom(getRandomCurrency());
        tradeMessage.setCurrencyTo(getRandomCurrencyExcept(tradeMessage.getCurrencyFrom()));
        tradeMessage.setAmountSell(RandomUtils.nextDouble(100, 10_000));
        tradeMessage.setAmountBuy(RandomUtils.nextDouble(100, 10_000));
        tradeMessage.setRate(tradeMessage.getAmountSell() / tradeMessage.getAmountBuy());
        tradeMessage.setTimePlaced(new Date());
        tradeMessage.setOriginatingCountry(getRandomCountryCode());
        return tradeMessage;
    }

    private Currency getRandomCurrencyExcept(Currency except) {
        Currency result;
        do {
            result = getRandomCurrency();
        } while (result == except);
        return result;
    }

    private CountryCode getRandomCountryCode() {
        return CountryCode.values()[RandomUtils.nextInt(0, CountryCode.values().length)];
    }

    private Currency getRandomCurrency() {
        return Currency.values()[RandomUtils.nextInt(0, Currency.values().length)];
    }

    private String generateRandomUserId() {
        return RandomStringUtils.randomNumeric(6);
    }
}
