package com.vkorobkov.integration.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import java.io.IOException;

@ThreadSafe
public class TradeMessagePoster {

    public static final String HOST = "http://localhost:8080";

    private static final RandomTradeMessageGenerator generator = new RandomTradeMessageGenerator();
    private static final ObjectMapper mapper = new ObjectMapper();

    public static int postRandomTradeMessage(CloseableHttpClient client) throws IOException {
        String jsonBody = mapper.writeValueAsString(generator.generateRandomTradeMessage());

        HttpPost post = new HttpPost(HOST + "/trade-message/");
        StringEntity jsonEntity = new StringEntity(jsonBody);
        jsonEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        post.setHeader("Content-type", "application/json");
        post.setEntity(jsonEntity);

        CloseableHttpResponse response = client.execute(post);
        try {
            return response.getStatusLine().getStatusCode();
        }
        finally {
            response.close();
        }
    }
}
