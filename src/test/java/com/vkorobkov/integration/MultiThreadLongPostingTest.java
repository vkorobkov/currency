package com.vkorobkov.integration;

import com.vkorobkov.integration.utils.TradeMessagePoster;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class MultiThreadLongPostingTest {

    private final static int TIMES_TO_POST = 1000;

    private final static int THREADS_TO_START = 3;

    @Test
    public void postManyTimes() throws IOException {
        long timeStart = System.currentTimeMillis();

        CloseableHttpClient client = HttpClients.custom().setMaxConnPerRoute(THREADS_TO_START).setMaxConnTotal(THREADS_TO_START).build();

        ExecutorService executor = Executors.newFixedThreadPool(THREADS_TO_START);
        for (int i = 0; i < THREADS_TO_START; ++i) {
            executor.submit(() -> {
                for (int j = 0; j < TIMES_TO_POST; ++j) {
                    int responseCode = TradeMessagePoster.postRandomTradeMessage(client);
                    Assert.assertEquals(200, responseCode);
                }
                return null;
            });
        }

        executor.shutdown();
        while (!executor.isTerminated()) {
            Thread.yield();
        }

        long timeTook = System.currentTimeMillis() - timeStart;
        log.warn("IOPS: " + (int)((double)TIMES_TO_POST * (double)THREADS_TO_START / ((double)timeTook / 1000.0) ));
    }
}
