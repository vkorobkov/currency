package com.vkorobkov.integration;


import com.vkorobkov.integration.utils.TradeMessagePoster;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class SingleTradeMessagePostTest {

    @Test
    public void generateRandomTraffic() throws IOException {
        int responseCode = TradeMessagePoster.postRandomTradeMessage(HttpClients.createDefault());
        Assert.assertEquals(200, responseCode);
    }
}